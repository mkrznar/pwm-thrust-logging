﻿/*
Name:		PWMThrustLogging.ino
Project:    TRIVIA LOGGING STATION
Created:	3/24/2018 8:23:47 PM
Author:		Matija Krznar

dijelovi programa
a) inicijalizacije
b) setup loop
c) razne custom funkcije
d) void loop
- program control function - main menu
-  odvojeno je racuanje (ocitavanje) funkcija currentState()
i ispis u konzolu; funkcija sendToSerial();
*/

// "log view studio" initialisation
boolean headersend = false; // kada je hedersend true onda se salju podaci u logview
boolean menuprinted = false; // ovo sluzi za main meni, da li krecemo prvi put ili zaustavljamo mjerenje
unsigned long intervalDurationInt = 10000; // interval at which to do something (milliseconds)				/ custom unos podataka
int numberofrepeats = 2; // broj poanvljanja pokusa
bool customPwmVector = false;

// za kalibraciju vagi
int scaleSelect;
float calibrationMass; // default mass for calibration
float scale1constant = 402.02279;
float scale2constant = 402.02279;
float scale3constant = 402.02279;
float scale4constant = 402.02279;
float scale1out, scale2out, scale3out, scale4out;

// timer and intervals 
unsigned long previousMillis = 0; // last time update
int counter = 0; // brojac za iteraciju kroz PWM
int ii = 0; // brojac za globalni broj ponavljanja
bool useDefaultPWMVector = true;

			// define PWM vector varijanta 1
/*
int pwmVectorDefault[] = 
	  { 1000, 1050, 1100, 1150, 1200, 1250, 1300, 1350, 1400,
		1450, 1500, 1550, 1600, 1650, 1700, 1750, 1800, 1850,
		1900, 1950, 2000, 1950, 1900, 1850, 1800, 1750, 1700,
		1650, 1600, 1550, 1500, 1450, 1400, 1350, 1300, 1250,
		1200, 1150, 1100, 1050, 1000 };
			int pwmVectorSizeDefault = 41;
			*/

			// define PWM vector varijanta 1
int pwmVectorDefault[] = { 1000, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800,
1900, 2000, 1900, 1800, 1700, 1600, 1500, 1400, 1300,
1200, 1100, 1000,1000 };
int pwmVectorSizeDefault = 23;

// Declaration of custom arrays for user input vector
int *pwmVectorCustom = 0;
int pwmVectorSizeCustom = 0;

// init vage
#include "HX711t.h"  // drugi library ide za teensy
HX711t scale1(6, 5);    // HX711.DOUT - pin #A1  HX711.PD_SCK - pin #A0  ---- VAGA 1
HX711t scale2(4, 3);    // HX711.DOUT - pin #A8  HX711.PD_SCK - pin #A7  ---- VAGA 2
HX711t scale3(10, 9);   // HX711.DOUT - pin #A1  HX711.PD_SCK - pin #A0  ---- VAGA 3
HX711t scale4(12, 11);  // HX711.DOUT - pin #A1  HX711.PD_SCK - pin #A0  ---- VAGA 4

#include "eeprom.h" // za citanje faktora kalibracije vage i kalibriranje
#include "math.h"

void setup()
{
	Serial.begin(9600); // init serial connection
	while (!Serial); // dok serial stream nije poceo ne radi nista
	mainMenu();
	analogWriteResolution(16); // 16 bits ersolution 
	analogWriteFrequency(3, 400); // output pin and frequency in Hz

	scale1.set_scale(scale1constant);    // this value is obtained by calibrating the scale with known weights; see the README for details
	scale2.set_scale(scale2constant);
	scale3.set_scale(scale3constant);
	scale4.set_scale(scale4constant);

	
}


void mainMenuText() {
	Serial.println("---------------------------");
	Serial.println("* TRIVIUM LOGGING STATION *");
	Serial.println("---------------------------");
	Serial.println("");
	Serial.println("Main menu");
	Serial.println("[S]tart mesurement");
	Serial.println("[E]nd mesurement");
	Serial.println("[P]WM vector definition");
	Serial.println("[W]hat is my PMW vector?");
	Serial.println("[T]ime interval defition for PWM step");
	Serial.println("[R]epeat mesurement, number of repeats");
	Serial.println("[C]alibration mode");
	Serial.println("[M]esure testing for Scales");

}

void mainMenu()
{	// ovo se pokrece nakon reseta mikrokontrolera /TBD
	if (menuprinted == false)
	{
		mainMenuText();
		menuprinted = true;
	};

	if (Serial.available() > 0)
	{
		char c = Serial.read();
		switch (c)
		{
		case 83: // "S" -> Start // ascii kod, broj 83 je S  http://www.theasciicode.com.ar/ascii-printable-characters/capital-letter-e-uppercase-ascii-code-69.html
			logViewInit();
			break;

		case 115: // "s" -> Start 
			logViewInit();
			break;

		case 69: // "E" -> End
			logViewTerminate();
			counter = 0;
			break;

		case 101: // "e" -> End
			logViewTerminate();
			counter = 0;
			break;

		case 80: // "P" -> pwm
			repeatmesurement(); // broj ponavljanja mjerenja
			getMesurementSize(); // definiraj velicinu vektora, koliko elemenata
			makeCustomVector(); // napuni vektor
			mainMenuText();
			useDefaultPWMVector = false;
			break;

		case 112: // "p" -> pwm
			repeatmesurement(); // broj ponavljanja mjerenja
			getMesurementSize(); // definiraj velicinu vektora, koliko elemenata
			makeCustomVector(); // napuni vektor
			mainMenuText();
			useDefaultPWMVector = false;
			break;

		case 87: // W
			whatIsMyPwmVector();
			mainMenuText();
			break;

		case 119: // w
			whatIsMyPwmVector();
			mainMenuText();
			break;

		case 84: // T definicija intervala trajana
			defineIntervalDuration();
			mainMenuText();
			break;

		case 116: // t definicija intervala trajana
			defineIntervalDuration();
			mainMenuText();
			break;

		case 82: // R broj ponavljanja 
			repeatmesurement();
			mainMenuText();
			break;

		case 114: // r broj ponavljanja 
			repeatmesurement();
			mainMenuText();
			break;

		case 67: //C calibration mode
			calibrateScale();
			mainMenuText();
			break;


		case 99: // c calibration mode
			calibrateScale();
			mainMenuText();
			break;

		case 77: // M test scale
			testScale();
			mainMenuText();
			break;
		}
	}
}

void logViewInit()
{
	String tempst;
	Serial.println("Start detected");
	// Send Header information
	tempst = "$N$;Data Logging\r\n";      // data logging je text hedera
	Serial.print(tempst);
	tempst = "$C$;PWM in ms; Grams 1; Grams 2; Grams 3 ; Grams 4\r\n";   // ovdje upisujemo vage
	Serial.print(tempst);
	scale1.tare(20);   // reset the scale to 0
	scale2.tare(20);
	scale3.tare(20);
	scale4.tare(20);
	headersend = true;
}

void logViewTerminate()
{
	Serial.println("End detected");
	headersend = false;
	menuprinted = false;  // ponovno ispisujem meni
}

void repeatmesurement()
{
	Serial.println("Unesite broj ponavljanja PWM vektora signala:");
	while (Serial.available() == 0) {}
	numberofrepeats = Serial.parseInt();
};

void getMesurementSize()
{	// define vector size
	Serial.println("Unesite broj koraka PWM signala: ");
	while (Serial.available() == 0) {}
	pwmVectorSizeCustom = Serial.parseInt();
	//ovim funkcijama allociram array na novi size 
	if (pwmVectorCustom != 0) {
		pwmVectorCustom = (int*)realloc(pwmVectorCustom, pwmVectorSizeCustom * sizeof(int));
	}
	else {
		pwmVectorCustom = (int*)malloc(pwmVectorSizeCustom * sizeof(int));
	}
}

void testScale() {
	int countTest = 0;
	Serial.println("*** TEST SCALE MODE ***");
	Serial.println("");
	Serial.println("What scale do you want to TEST? Input 1 for scale 1, 2 for scale 2...");
	while (Serial.available() == 0) {}
	scaleSelect = Serial.parseInt();
	Serial.println("Starting testing for scale no: ");
	Serial.println(scaleSelect);
	scale2.set_scale(scale2constant);
	while (countTest < 1000) {
		//delay(1);        // delay in between reads for stability
		Serial.print("mesurement:  ");
		Serial.println(scale2.get_units());
		countTest++;
	}
}

void calibrateScale() {
	Serial.println("*** CALIBRATION MODE ***");
	Serial.println("");
	Serial.println("What scale do you want to calibrate? Input 1 for scale 1, 2 for scale 2...");
	while (Serial.available() == 0) {}
	scaleSelect = Serial.parseInt();
	Serial.println("Starting calibration for scale no: ");
	Serial.println(scaleSelect);
	startCalibrationScale(scaleSelect); // pokrecemo funkciju koja kalibrira
};


void startCalibrationScale(int scaleSelect) {
	int count = 0; // conter
	int mesurement_steps = 100;
	char progressBar[10] = { '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' };

	if (scaleSelect == 1) {
		scale1.set_scale();                    // this value is obtained by calibrating the scale with known weights; see the README for details
		scale1.tare(); // reset the scale to 0

		
	} else if (scaleSelect == 2) {
		scale2.set_scale();                    // this value is obtained by calibrating the scale with known weights; see the README for details
		scale2.tare(5); // reset the scale to 0, take 5 mesurement to calm down initial fluctuations
		Serial.println("Input value of your known mass:  ");
		Serial.println("(HINT* If you enter mass in grams, output will be in grams, if in KG, output is KG, etc.");
		while (Serial.available() == 0) {}
		calibrationMass = Serial.parseFloat();
		
		
		Serial.println("Put mass on scale and send C into console to begin calibration...");
		while (Serial.available() == 0) {}
		char startCalibration = Serial.read();
		
		// calibration proces
		if (startCalibration == 67) {
			
			float total;
			float total_prev;
			float average;
			float percentege;
			
			while (count < mesurement_steps) {
				//delay(1);        // delay in between reads for stability
				total = total_prev + scale2.read();
				count++;
				average = total / count;
				total_prev = total;
				percentege = count * 100 / mesurement_steps;
					
				if (0 <= percentege && percentege < 10) {
						progressBar[0] = '*';
					}
					else if (10 <= percentege && percentege < 20) {
						progressBar[0] = '*';
						progressBar[1] = '*';
					}
					else if (20 <= percentege && percentege < 30) {
						progressBar[0] = '*';
						progressBar[1] = '*';
						progressBar[2] = '*';
					}
					else if (30 <= percentege && percentege < 40) {
						progressBar[0] = '*';
						progressBar[1] = '*';
						progressBar[2] = '*';
						progressBar[3] = '*';
					}
					else if (40 <= percentege && percentege < 50) {
						progressBar[0] = '*';
						progressBar[1] = '*';
						progressBar[2] = '*';
						progressBar[3] = '*';
						progressBar[4] = '*';
											}
					else if (50 <= percentege && percentege < 60) {
						progressBar[0] = '*';
						progressBar[1] = '*';
						progressBar[2] = '*';
						progressBar[3] = '*';
						progressBar[4] = '*';
						progressBar[5] = '*';
					}
					else if (60 <= percentege && percentege < 70) {
						progressBar[0] = '*';
						progressBar[1] = '*';
						progressBar[2] = '*';
						progressBar[3] = '*';
						progressBar[4] = '*';
						progressBar[5] = '*';
						progressBar[6] = '*';
					}
					else if (70 <= percentege && percentege < 80) {
						progressBar[0] = '*';
						progressBar[1] = '*';
						progressBar[2] = '*';
						progressBar[3] = '*';
						progressBar[4] = '*';
						progressBar[5] = '*';
						progressBar[6] = '*';
						progressBar[7] = '*';
					}
					else if (80 <= percentege && percentege < 90) {
						progressBar[0] = '*';
						progressBar[1] = '*';
						progressBar[2] = '*';
						progressBar[3] = '*';
						progressBar[4] = '*';
						progressBar[5] = '*';
						progressBar[6] = '*';
						progressBar[7] = '*';
						progressBar[8] = '*';
					}
					else if (90 <= percentege && percentege <= 100) {
						progressBar[0] = '*';
						progressBar[1] = '*';
						progressBar[2] = '*';
						progressBar[3] = '*';
						progressBar[4] = '*';
						progressBar[5] = '*';
						progressBar[6] = '*';
						progressBar[7] = '*';
						progressBar[8] = '*';
						progressBar[9] = '*';
					}
					


				Serial.print(scale2.read());
				Serial.print(" (RAW READING)      ");
				Serial.print(average);
				Serial.print(" (AVERAGE)       Progress %: ");
				Serial.print(percentege);
				Serial.print("    ");
				Serial.println(progressBar);


				// instant averageing
			}

			// obtained scale factor
			Serial.println("Obtained scale factor is");
			Serial.println(average / calibrationMass);

			scale2constant = average / calibrationMass;
			scale2.set_scale(scale2constant);
		}


		} else if (scaleSelect == 3) {
		 scale3.set_scale();                    // this value is obtained by calibrating the scale with known weights; see the README for details
		 scale3.tare(); // reset the scale to 0

		} else if (scaleSelect == 4) {
			scale4.set_scale();                    // this value is obtained by calibrating the scale with known weights; see the README for details
			scale4.tare(); // reset the scale to 0
		}

		
	
}


// funkcija za definiranje intervala koraka pwma u milisekundama
void defineIntervalDuration()
{
	// define interval duration in miliseconds
	Serial.println("Unesite trajanje intervala jednog koraka PWM (u milisekundama): ");
	while (Serial.available() == 0) {}
	intervalDurationInt = Serial.parseInt();
}

// funkcija za punjenje custom vektora signala pwma
void makeCustomVector()
{// upisi u generirani vektor
	customPwmVector = true;
	for (int i = 0; i < pwmVectorSizeCustom; i++)
	{
		Serial.print("Upisite PWM br ");
		Serial.print(i);
		Serial.print(":");
		while (Serial.available() == 0) {}
		pwmVectorCustom[i] = Serial.parseInt();
	}
}

// funkcija za citanje PWM i ostalog stanja
void whatIsMyPwmVector() {
	Serial.println("Broj ponavljanja ulaznog signala");
	Serial.println(numberofrepeats);

	Serial.println("velicina vektora pwm-a (kolicina varijabli u njemu)");
	if (customPwmVector == true) {
		Serial.println(pwmVectorSizeCustom);
	}
	else { Serial.println(pwmVectorSizeDefault); };

	Serial.println("trajanje jednog intervala");
	Serial.println(intervalDurationInt);
	//pwmCounter = 0; // u ovom trenu stavimo counter na nula jer smo definirali interval
	Serial.println("ispis vektora PMW:");
	if (customPwmVector == true) {
		for (int i = 0; i < pwmVectorSizeCustom; i++)
		{
			Serial.println(pwmVectorCustom[i]);
		};
	}
	else
	{
		for (int i = 0; i < pwmVectorSizeDefault; i++)
		{
			Serial.println(pwmVectorDefault[i]);
		}
	};



}

// funkcija za generiranje PWM, cmd je od 1000 do 2000 
void sendPWM(float Cmd) {
	analogWrite(3, Cmd / 2500.0f*65535.0f);
}

// funkcija za iteriranje po pwm vektoru
void sendPWMvector() {
	
	if (useDefaultPWMVector == true)
	{
		sendPWM(pwmVectorDefault[counter]);

		unsigned long currentMillis = millis();   // uzmemo trenutno vrijeme
		if (currentMillis - previousMillis >= intervalDurationInt)
		{
			// save the last we changed state
			previousMillis = currentMillis;  // trenutno postaje proslo
			counter = counter + 1;

			if (counter == pwmVectorSizeDefault) {
				counter = 0;
				//logViewTerminate();
				//mainMenu();
			}
			
		}
		
	}



	if (useDefaultPWMVector == false)
	{
		sendPWM(pwmVectorCustom[counter]);
		unsigned long currentMillis = millis();   // uzmemo trenutno vrijeme
		if (currentMillis - previousMillis >= intervalDurationInt)
		{
			// save the last we changed state
			previousMillis = currentMillis;  // trenutno postaje proslo
			counter = counter + 1;

			if (counter == pwmVectorSizeCustom) {
				counter = 0;
				//logViewTerminate();
				//mainMenu();
			}
		}
	}
}
// funkcija za  ispis u konzolu u LogView Formatu;  sendToSerial();

void takeMesurementsFromScales() {
	scale1out = scale1.get_units(3);

	scale2out = scale2.get_units(3);

	scale3out = scale3.get_units(3);

	scale4out = scale4.get_units(3);

}

void sendToSerial()
{
	// TO BE DONE, provjeri ispisivanje u konzolu....
	//Serial.print(pwmVectorSizeDefault);
	//Serial.print(pwmVectorSizeCustom);

	Serial.print("$");
	Serial.print(pwmVectorDefault[counter]);
	Serial.print(";");
	Serial.print(scale1out);   
	Serial.print(";");
	Serial.print(scale2out);
	Serial.print(";");
	Serial.print(scale3out);
	Serial.print(";");
	Serial.print(scale4out);
	Serial.print("\r\n");

	/*

	if (pwmVectorSizeDefault == true) {
		Serial.print("$");
		Serial.print(pwmVectorDefault[counter]);
		Serial.print(";");
		Serial.print(scale1.get_units());   // brojac pwma
		Serial.print(";");
		Serial.print(scale2.get_units());
		Serial.print(";");
		Serial.print(scale3.get_units());
		Serial.print(";");
		Serial.print(scale4.get_units());
		Serial.print("\r\n");
	};

	if (pwmVectorSizeCustom == true)
	{
		Serial.print("$");
		Serial.print(pwmVectorCustom[counter]);
		Serial.print(";");
		Serial.print(scale1.get_units());   // brojac pwma
		Serial.print(";");
		Serial.print(scale2.get_units());   //
		Serial.print(";");
		Serial.print(scale3.get_units());   //
		Serial.print(";");
		Serial.print(scale4.get_units());   //
		Serial.print("\r\n");
	};

	*/
}

// funkcija za plotanje logview  
void mesurementsAndPlots()
{
	if (headersend)
	{
		//sendPWMvector(); // slanje pwm vektora 
		takeMesurementsFromScales(); // mjerenje s vagama
		sendToSerial(); // plotting
	}
}

void loop()
{
	mainMenu();
	mesurementsAndPlots();


}